public class TestRectangle {
    
    // Calculate Area
    public static double calculateArea(double len, double bre) {
        System.out.println("Calculating area of rectangle with");
        System.out.println("length = " + len + " cm and breadth = " + bre + " cm");
        double area = len * bre;
        return area;
    }

    // Test rectangle is square
    public static boolean checkEqualSides(double len, double bre) {
        System.out.println("Checking sides of rectangle");
        System.out.println("length = " + len + " cm and breadth = " + bre + " cm");
        if ( len == bre ) {
            return true;
        } else 
            return false;
    }
    
    public static void main(String args[]) {
        double areaOfRectangle = calculateArea(12.156, 31.212);
        boolean checkSquare = checkEqualSides(13.12, 11.11);  
        System.out.println("Area of rectangle = " + areaOfRectangle + " cm sq");
        System.out.println("Check Square = " + checkSquare); 
    }
}