public class TestAssignments {
    public static void main(String args[]) {
        int a = 10;
        int b = 20;
        int c = 30;

        c = a + b;
        System.out.println("c = a + b = " + c );

        c += a;
        System.out.println("c = c + a = " + c);

        c -= a;
        System.out.println("c = c - a = " + c);
    }
}