public class TestRelational {
    
    public static void printRelationals(int a, int b) {
        System.out.println("a == b = " + (a == b)); // Equal to
        System.out.println("a != b = " + (a != b)); // Not equal to
        System.out.println("a > b = "  + (a > b)); // greater than
        System.out.println("a < b = " + (a < b)); // lesser than
        System.out.println("a >=    = " + (a >= b)); // greater than or equal to
        System.out.println("a <=    = " + (a <= b)); // lesser than or equal to
    }    
    public static void main(String args[]) {
        //int a = 10,
        //    b = 20;
        printRelationals(10, 20);
        System.out.println("==========");
        printRelationals(45, 28);        
    }
}