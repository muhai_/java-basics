// Create a new program that contains three methods 
// i: Calculate the perimteter of a triangle
// ii: Calculate the area of a triangle
// iii: Test if a triangle is equalateral, and provide a result of true or false

class TestTriangle {
    
    // Calculate perimeter
    public static double calcPerimeter(double a, double b, double c) {
        double perimeter = a + b + c;
        return perimeter;
    }
    
    // Calculate area of triangle 
    public static double calcArea(double base, double height) {
        double area = (1/2 * base * height);
        return area;
    }
    
    public static void main(String args[]) {
        double checkPerimeter = calcPerimeter(10, 10, 10);
        double checkArea = calcArea(10, 20);
        System.out.println("Perimter of triangle is " + checkPerimeter);
        System.out.println("Perimter of triangle is " + checkArea);
    };
}