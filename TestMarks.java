import java.util.Scanner;

public class TestMarks {
    public static void main(final String args[]) {
        System.out.println("Enter Marks :");
        Scanner sc = new Scanner(System.in);
        int marks = sc.nextInt();        
        // Check marks below 50
        if (marks < 50) {
            System.out.println("You have failed");
        }
        // Check marks leeser than or equal 50 to greater than or equal 59
        else if (marks >= 50 && marks <= 59) {
            System.out.println("You score a C");
        }
        // Check marks leeser than or equal 60 to greater than or equal 69
        else if (marks >= 60 && marks <= 69) {
            System.out.println("You score a C");
        }
        // Check marks leeser than or equal 70 to greater than or equal 79
        else if (marks >= 70 && marks <= 79) {
            System.out.println("You score a B");
        }
        // Any input after 79 is A
        else {
            System.out.println("You score a A");
        }
        sc.close();
    }
}