import java.util.Scanner;

public class TestLogin {
    public static void main(String args[]) {
        // declare variables
        boolean login = false;
        String password = "mypass";
        System.out.println("Enter password :");
        // Call scanner 
        Scanner scanner = new Scanner(System.in);
        // Set scanner input
        String input = scanner.nextLine();
        // Condition Statement
        if(input.equals(password)) {
            login = true;
            System.out.println("Login success = " + login);
            System.out.println("Welcome to the jungle!");
        } else {
            System.out.println("Login success = " + login);
            System.out.println("You are not authorised");
        }
        scanner.close();
    }
}